using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Entities
{
    public class Venda
    {
        public int Id { get; set; }
        public string NomeDoItem { get; set; }
        public string Descricao { get; set; }
        public EnumStatusVenda Pagamento { get; set; }
        public EnumStatusEntrega Entrega { get; set; }


    }
}