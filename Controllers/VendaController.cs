using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Entities;
using tech_test_payment_api.context;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("controller")]
    public class VendaController : ControllerBase
    {
        private readonly VendasContext _context;
        public VendaController(VendasContext context)
        {
            _context = context;
        }
        [HttpPost]
        public IActionResult Criar(Venda venda)
        {
            _context.Add(venda);
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterPorId), new { id = venda.Id }, venda);
        }
        [HttpGet("ObterPorId")]
        public IActionResult ObterPorId(int id)
        {
            var venda = _context.Vendas.Find(id);
            if (venda == null)
                return NotFound();
            return Ok(venda);
        }
        [HttpGet("ObterPorNome")]
        public IActionResult ObterPorNome(string NomeDoItem)
        {
            var Vendas = _context.Vendas.Where(XmlConfigurationExtensions => XmlConfigurationExtensions.NomeDoItem.Contains(NomeDoItem));
            return Ok(Vendas);
        } 
        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, Venda Venda)
        {
            var VendaBanco = _context.Vendas.Find(id);

            if(VendaBanco == null)
                return NotFound();

            VendaBanco.NomeDoItem = Venda.NomeDoItem;
            VendaBanco.Descricao = Venda.Descricao;
            VendaBanco.Pagamento = Venda.Pagamento;
            VendaBanco.Entrega = Venda.Entrega;

            _context.Vendas.Update(VendaBanco);
            _context.SaveChanges();

            return Ok(VendaBanco);
        }     
        [HttpDelete("{id}")]
        public IActionResult Deletar(int id)
        {
            var VendaBanco = _context.Vendas.Find(id);

            if(VendaBanco == null)
                return NotFound();

            _context.Vendas.Remove(VendaBanco);
            _context.SaveChanges();

            return NoContent();
        } 
    }
}